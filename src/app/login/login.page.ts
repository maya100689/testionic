import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor() { }

  IniciarSesion(formulario: NgForm){
    firebase.auth().signInWithEmailAndPassword(formulario.value.correo, formulario.value.clave).
    then(info => console.log(info)).
    catch( error => console.log(error)
    );


  }
registrarUsuario(formulario: NgForm){
  firebase.auth().createUserWithEmailAndPassword(formulario.value.correo, formulario.value.clave).
  then(info => console.log(info)).
  catch( error => console.log(error)
  );
}
  ngOnInit() {
  }

}
