import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase';
import { timingSafeEqual } from 'crypto';

@Component({
  selector: 'app-mostrar-info',
  templateUrl: './mostrar-info.page.html',
  styleUrls: ['./mostrar-info.page.scss'],
})
export class MostrarInfoPage implements OnInit {

  public id;
  public nombre:any = null;
  public edad:any = null;
  constructor() {

    this.id = 0;
    this.nombre = "";
    this.edad = 0;
   }

  mostrarInfo(){

    //var userId = firebase.auth().currentUser.uid;
    var database = firebase.database()
    var ref = database.ref('usuario').once('value').then((data) =>{
    
      var info = data.val();
      return info;
      //this.id = data.val().id;
      //this.nombre = data.val().nombre;
      //this.edad = data.val().edad;

    });
    ref.then((info)=>{

      this.id = info.id;
      this.nombre= info.nombre;
      this.edad = info.edad;
    });


    //console.log(ref );
  }

  ngOnInit() {
  }

}
