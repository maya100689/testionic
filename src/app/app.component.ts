import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();

    //firebase connection
    var app = firebase.initializeApp({

      apiKey: "AIzaSyA2wxe0FyzD3poNelYfno2QnuZSdgaVoeI",
      authDomain: "bdtest-88b8c.firebaseapp.com",
      databaseURL: "https://bdtest-88b8c.firebaseio.com",
      projectId: "bdtest-88b8c",
      storageBucket: "bdtest-88b8c.appspot.com",
      messagingSenderId: "602463577016"

    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
